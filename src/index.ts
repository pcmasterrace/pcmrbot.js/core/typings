import * as FastestValidator from "./fastest-validator/index";
import * as Typings from "./typings/index";

export {
    FastestValidator,
    Typings
}