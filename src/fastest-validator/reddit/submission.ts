import { ActionParams } from "moleculer";

export const Submission: ActionParams = {
    approved: {
        type: "boolean",
        convert: true
    },
    approvedAt: {
        type: "date",
        convert: true,
        optional: true
    },
    approvedBy: {
        type: "string",
        optional: true
    },
    archived: {
        type: "boolean",
        convert: true
    },
    author: {
        type: "object",
        props: {
            flair: {
                type: "object",
                props: {
                    cssClass: {
                        type: "string",
                        optional: true
                    },
                    templateId: {
                        type: "string",
                        optional: true
                    },
                    text: {
                        type: "string",
                        optional: true
                    }
                }
            },
            name: {
                type: "string",
                optional: true
            },
            username: "string"
        }
    },
    // TODO: Fill this out later with keys
    awards: {
        type: "array"
    },
    body: "string",
    commentCount: {
        type: "number",
        integer: true
    },
    contestMode: {
        type: "boolean",
        convert: true
    },
    created: {
        type: "date",
        convert: true
    },
    distinguished: {
        type: "boolean",
        convert: true
    },
    domain: "string",
    edited: {
        type: "boolean",
        convert: true
    },
    editedAt: {
        type: "date",
        convert: true,
        optional: true
    },
    flair: {
        type: "object",
        props: {
            cssClass: {
                type: "string",
                optional: true
            },
            templateId: {
                type: "string",
                optional: true
            },
            text: {
                type: "string",
                optional: true
            }
        }
    },
    gilded: "number",
    isSelf: {
        type: "boolean",
        convert: true
    },
    isVideo: {
        type: "boolean",
        convert: true
    },
    locked: {
        type: "boolean",
        convert: true
    },
    media: {
        type: "object",
        optional: true,
        props: {
            embed: {
                type: "object",
                optional: true,
                props: {
                    author: {
                        type: "object",
                        props: {
                            name: "string",
                            url: "url"
                        }
                    },
                    provider: {
                        type: "object",
                        props: {
                            name: "string",
                            url: "url"
                        }
                    },
                    thumbnailUrl: "url",
                    title: "string",
                    type: "string"
                }
            },
            redditVideo: {
                type: "object",
                optional: true,
                props: {
                    dashUrl: "url",
                    duration: "number",
                    fallbackUrl: "url",
                    hlsUrl: {
                        type: "url",
                        optional: true
                    },
                    isGif: "boolean",
                    transcodingStatus: "string"
                }
            },
            type: {
                type: "string",
                optional: true
            }
        }
    },
    modNote: {
        type: "object",
        props: {
            author: {
                type: "string",
                optional: true
            },
            note: {
                type: "string",
                optional: true
            },
            title: {
                type: "string",
                optional: true
            }
        }
    },
    name: {
        type: "string",
        empty: false
    },
    nsfw: {
        type: "boolean",
        convert: true
    },
    permalink: "string",
    pinned: {
        type: "boolean",
        convert: true
    },
    preview: {
        type: "object",
        optional: true
    },
    removalReason: {
        type: "string",
        optional: true
    },
    removed: {
        type: "boolean",
        convert: true
    },
    removedAt: {
        type: "date",
        convert: true,
        optional: true
    },
    removedBy: [
        {
            type: "string",
            optional: true
        }, {
            type: "boolean",
            convert: true,
            optional: true
        }
    ],
    reports: {
        type: "object",
        props: {
            count: "number",
            dismissed: {
                type: "array",
                items: {
                    type: "object",
                    props: {
                        count: "number",
                        reason: {
                            type: "string",
                            optional: true
                        }
                    }
                }
            },
            ignore: {
                type: "boolean",
                convert: true, 
                default: false
            },
            mod: {
                type: "array",
                items: {
                    type: "object",
                    props: {
                        mod: "string",
                        reason: {
                            type: "string",
                            optional: true
                        }
                    }
                }
            },
            user: {
                type: "array",
                items: {
                    type: "object",
                    props: {
                        count: "number",
                        reason: {
                            type: "string",
                            optional: true
                        }
                    }
                }
            }
        }
    },
    score: "number",
    spam: {
        type: "boolean",
        convert: true
    },
    spoiler: {
        type: "boolean",
        convert: true
    },
    stickied: {
        type: "boolean",
        convert: true
    },
    subreddit: {
        type: "string",
        empty: false
    },
    suggestedSort: {
        type: "string",
        optional: true
    },
    thumbnail: {
        type: "string",
        optional: true
    },
    title: {
        type: "string",
        empty: false
    },
    url: "url",
    viewCount: {
        type: "number",
        integer: true,
        min: 0
    }
}