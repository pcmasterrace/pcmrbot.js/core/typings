import { ActionParams } from "moleculer";

export const WikiPage: ActionParams = {
    body: "string",
    mayRevise: {
        type: "boolean",
        convert: true
    },
    revision: {
        type: "object",
        props: {
            by: {
                type: "string",
                optional: true
            },
            date: {
                type: "date",
                convert: true,
                optional: true
            },
            id: {
                type: "string",
                optional: true
            },
            reason: {
                type: "string",
                optional: true
            }
        }
    }
}