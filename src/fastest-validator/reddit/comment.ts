import { ActionParams } from "moleculer";

export const Comment: ActionParams = {
    approved: {
        type: "boolean",
        convert: true
    },
    approvedAt: {
        type: "date",
        convert: true,
        optional: true
    },
    approvedBy: {
        type: "string",
        optional: true
    },
    archived: {
        type: "boolean",
        convert: true
    },
    author: {
        type: "object",
        props: {
            flair: {
                type: "object",
                props: {
                    cssClass: {
                        type: "string",
                        optional: true
                    },
                    templateId: {
                        type: "string",
                        optional: true
                    },
                    text: {
                        type: "string",
                        optional: true
                    }
                }
            },
            name: {
                type: "string",
                optional: true
            },
            isSubmitter: {
                type: "boolean",
                convert: true
            },
            username: "string"
        }
    },
    // TODO: Fill this out later
    awards: {
        type: "array"
    },
    body: "string",
    created: {
        type: "date",
        convert: true
    }, 
    distinguished: {
        type: "boolean",
        convert: true
    },
    edited: {
        type: "boolean",
        convert: true
    },
    editedAt: {
        type: "date",
        convert: true,
        optional: true
    },
    locked: {
        type: "boolean",
        convert: true
    },
    modNote: {
        type: "object",
        props: {
            author: {
                type: "string",
                optional: true
            },
            note: {
                type: "string",
                optional: true
            },
            title: {
                type: "string",
                optional: true
            }
        }
    },
    name: {
        type: "string",
        empty: false
    },
    nsfw: {
        type: "boolean",
        convert: true
    },
    parentName: {
        type: "string",
        optional: true
    },
    parentSubmission: {
        type: "object",
        props: {
            author: {
                type: "string",
                empty: false,
                optional: true
            },
            comments: {
                type: "number",
                optional: true
            },
            name: {
                type: "string",
                empty: false,
                optional: true
            },
            permalink: {
                type: "string",
                empty: false,
                optional: true
            },
            title: {
                type: "string",
                empty: false,
                optional: true
            },
            url: {
                type: "url",
                optional: true
            }
        }
    },
    permalink: "string",
    removalReason: {
        type: "string",
        optional: true
    },
    removed: {
        type: "boolean",
        convert: true
    },
    removedAt: {
        type: "date",
        convert: true,
        optional: true
    },
    removedBy: [
        {
            type: "string",
            optional: true
        }, {
            type: "boolean",
            convert: true,
            optional: true
        }
    ],
    reports: {
        type: "object",
        props: {
            count: "number",
            dismissed: {
                type: "array",
                items: {
                    type: "object",
                    props: {
                        count: "number",
                        reason: {
                            type: "string",
                            optional: true
                        }
                    }
                }
            },
            ignore: {
                type: "boolean",
                convert: true, 
                default: false
            },
            mod: {
                type: "array",
                items: {
                    type: "object",
                    props: {
                        mod: "string",
                        reason: {
                            type: "string",
                            optional: true
                        }
                    }
                }
            },
            user: {
                type: "array",
                items: {
                    type: "object",
                    props: {
                        count: "number",
                        reason: {
                            type: "string",
                            optional: true
                        }
                    }
                }
            }
        }
    },
    // If only this supported recursive validation...
    replies: {
        type: "array"
    },
    score: {
        type: "number",
        integer: true
    },
    scoreHidden: {
        type: "boolean",
        convert: true
    },
    spam: {
        type: "boolean",
        convert: true
    },
    stickied: {
        type: "boolean",
        convert: true
    },
    subreddit: {
        type: "string",
        empty: false
    }
}