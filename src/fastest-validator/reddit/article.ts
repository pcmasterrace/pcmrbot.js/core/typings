import { ActionParams } from "moleculer";

import { Submission } from "./submission";
import { Comment } from "./comment"; 

export const Article: ActionParams = {
    submission: {
        type: "object",
        props: Submission
    },
    comments: {
        type: "array",
        items: {
            type: "object",
            props: Comment
        }
    }
}