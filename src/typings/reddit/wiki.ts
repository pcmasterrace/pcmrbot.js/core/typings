export interface WikiPage {
    body: string,
    mayRevise: boolean,
    revision: {
        by: string | null,
        date: Date | null,
        id: string | null,
        reason: string | null
    }
}