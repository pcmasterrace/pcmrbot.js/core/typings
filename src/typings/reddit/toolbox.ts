export interface ToolboxConfig {
    ver: number,
    domainTags: {
        name: string,
        color: string
    },
    removalReasons: {
        pmsubject: string,
        logreason: string,
        header: string,
        footer: string,
        logsub: string,
        logtitle: string,
        bantitle: string,
        getfrom: string,
        reasons: Array<{
            flairCSS: string,
            flairText: string,
            text: string,
            title: string
        }>
    },
    modMacros: Array<{
        text: string,
        title: string,
        distinguish?: boolean,
        ban?: boolean,
        remove?: boolean,
        approve?: boolean,
        lockthread?: boolean,
        sticky?: boolean,
        archivemodmail?: boolean,
        highlightmodmail?: boolean
    }>
}