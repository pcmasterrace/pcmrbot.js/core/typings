import { Submission } from "./submission";

export interface Subreddit {
    activeUsers: number,
    advertiserCategory: string,
    allOriginalContent: boolean,
    allowDiscovery: boolean,
    allowImages: boolean,
    allowVideoGifs: boolean,
    allowVideos: boolean,
    banner: {
        background: {
            color: string,
            image: string,
        },
        height: number | null,
        image: string,
        mobileImage: string,
        width: number | null, 
    },
    canAssignLinkFlair: boolean,
    canAssignUserFlair: boolean,
    collapseDeletedComments: boolean,
    collectionsEnabled: boolean,
    commentScoreHideMins: number,
    communityIcon: string,
    created: Date,
    description: string,
    disableContributorRequests: boolean,
    displayName: string,
    emojis: {
        enabled: boolean,
        height: number | null,
        width: number | null,
    },
    eventPostsEnabled: boolean,
    freeformReports: boolean,
    hasMenuWidget: boolean,
    header: {
        image: string,
        height: number,
        title: string,
        width: number 
    },
    icon: {
        image: string,
        height: number,
        width: number
    },
    isCrosspostable: boolean,
    isEnrolledInNewModmail: boolean,
    keyColor: string,
    lang: string,
    linkFlair: {
        enabled: boolean,
        position: string
    },
    name: string,
    nsfw: boolean,
    originalContentTagEnabled: boolean,
    primaryColor: string,
    publicDescription: string,
    publicTraffic: boolean,
    quarantine: boolean,
    restrictCommenting: boolean,
    restrictPosting: boolean,
    spoilersEnabled: boolean,
    submissionType: string,
    submitLinkLabel: string,
    submitText: string,
    submitTextLabel: string,
    subredditType: string,
    subscribers: number,
    suggestedCommentSort: string | null,
    title: string,
    url: string,
    wikiEnabled: boolean
}

export interface SubredditIndex extends Array<Submission> {}