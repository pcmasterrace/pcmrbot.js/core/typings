export interface Submission {
    approved: boolean,
    approvedAt: Date | null,
    approvedBy: string | null,
    archived: boolean,
    author: {
        flair: {
            cssClass: string | null,
            templateId: string | null,
            text: string | null
        },
        name?: string,
        username: string
    },
    awards: {}, // TODO: Fill this out later with keys
    body: string,
    commentCount: number,
    contestMode: boolean,
    created: Date,
    distinguished: boolean,
    domain: string,
    edited: boolean,
    editedAt?: Date,
    flair: {
        cssClass: string | null,
        templateId: string | null,
        text: string | null
    },
    gilded: number,
    isSelf: boolean,
    isVideo: boolean,
    locked: boolean,
    media?: {
        embed?: {
            author: {
                name: string,
                url: string
            },
            provider: {
                name: string,
                url: string
            },
            thumbnailUrl: string,
            title: string,
            type: string
        },
        redditVideo?: {
            dashUrl: string,
            duration: number,
            fallbackUrl: string,
            hlsUrl?: string,
            isGif: boolean,
            transcodingStatus: string
        }
        type: string
    },
    modNote: {
        author: string | null,
        note: string | null,
        title: string | null
    },
    name: string,
    nsfw: boolean,
    permalink: string,
    pinned: boolean,
    preview: any | null,
    removalReason: string | null,
    removed: boolean,
    removedAt: Date | null,
    removedBy: string | boolean | null,
    reports: {
        count: number,
        dismissed: Array<{
            count: number,
            reason?: string
        }>,
        ignore: boolean,
        mod: Array<{
            mod: string,
            reason?: string
        }>,
        user: Array<{
            count: number,
            reason?: string
        }>
    },
    score: number,
    spam: boolean,
    spoiler: boolean,
    stickied: boolean,
    subreddit: string,
    suggestedSort: string | null,
    thumbnail: string,
    title: string,
    url: string | null,
    viewCount: number,
}