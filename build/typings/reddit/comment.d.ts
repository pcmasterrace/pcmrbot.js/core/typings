export interface Comment {
    approved: boolean;
    approvedAt: Date | null;
    approvedBy: string | null;
    archived: boolean;
    author: {
        flair: {
            cssClass: string | null;
            templateId: string | null;
            text: string | null;
        };
        name?: string;
        isSubmitter: boolean;
        username: string;
    };
    awards: {};
    body: string;
    created: Date;
    distinguished: boolean;
    edited: boolean;
    editedAt: Date;
    locked: boolean;
    modNote: {
        author: string | null;
        note: string | null;
        title: string | null;
    };
    name: string;
    nsfw: boolean;
    parentName: string | null;
    parentSubmission: {
        author: string | null;
        comments: number | null;
        name: string | null;
        permalink: string | null;
        title: string | null;
        url: string | null;
    };
    permalink: string;
    removalReason: string | null;
    removed: boolean;
    removedAt: Date | null;
    removedBy: string | boolean | null;
    reports: {
        count: number;
        dismissed: Array<{
            count: number;
            reason?: string;
        }>;
        ignore: boolean;
        mod: Array<{
            mod: string;
            reason?: string;
        }>;
        user: Array<{
            count: number;
            reason?: string;
        }>;
    };
    replies: Comment[];
    score: number;
    scoreHidden: boolean;
    spam: boolean;
    stickied: boolean;
    subreddit: string;
}
