import { Comment } from "./comment";
import { Submission } from "./submission";
export interface Article {
    submission: Submission;
    comments: Comment[];
}
