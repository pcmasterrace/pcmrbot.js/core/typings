"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WikiPage = void 0;
exports.WikiPage = {
    body: "string",
    mayRevise: {
        type: "boolean",
        convert: true
    },
    revision: {
        type: "object",
        props: {
            by: {
                type: "string",
                optional: true
            },
            date: {
                type: "date",
                convert: true,
                optional: true
            },
            id: {
                type: "string",
                optional: true
            },
            reason: {
                type: "string",
                optional: true
            }
        }
    }
};
