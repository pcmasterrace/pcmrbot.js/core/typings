"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Article = void 0;
const submission_1 = require("./submission");
const comment_1 = require("./comment");
exports.Article = {
    submission: {
        type: "object",
        props: submission_1.Submission
    },
    comments: {
        type: "array",
        items: {
            type: "object",
            props: comment_1.Comment
        }
    }
};
