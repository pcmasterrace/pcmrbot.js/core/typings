"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ToolboxConfig = void 0;
exports.ToolboxConfig = {
    ver: {
        type: "number",
        positive: true,
        integer: true
    },
    domainTags: {
        type: "object",
        props: {
            name: "string",
            color: "string"
        }
    },
    removalReasons: {
        type: "object",
        props: {
            pmsubject: "string",
            logreason: "string",
            header: "string",
            footer: "string",
            logsub: "string",
            logtitle: "string",
            bantitle: "string",
            getfrom: "string",
            reasons: {
                type: "array",
                items: {
                    type: "object",
                    props: {
                        flairCSS: "string",
                        flairText: "string",
                        text: "string",
                        title: "string"
                    }
                }
            }
        }
    },
    modMacros: {
        type: "array",
        items: {
            type: "object",
            props: {
                text: "string",
                title: "string",
                distinguish: {
                    type: "boolean",
                    convert: true,
                    optional: true
                },
                ban: {
                    type: "boolean",
                    convert: true,
                    optional: true
                },
                remove: {
                    type: "boolean",
                    convert: true,
                    optional: true
                },
                approve: {
                    type: "boolean",
                    convert: true,
                    optional: true
                },
                lockthread: {
                    type: "boolean",
                    convert: true,
                    optional: true
                },
                sticky: {
                    type: "boolean",
                    convert: true,
                    optional: true
                },
                archivemodmail: {
                    type: "boolean",
                    convert: true,
                    optional: true
                },
                highlightmodmail: {
                    type: "boolean",
                    convert: true,
                    optional: true
                }
            }
        }
    }
};
