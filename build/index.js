"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Typings = exports.FastestValidator = void 0;
const tslib_1 = require("tslib");
const FastestValidator = tslib_1.__importStar(require("./fastest-validator/index"));
exports.FastestValidator = FastestValidator;
const Typings = tslib_1.__importStar(require("./typings/index"));
exports.Typings = Typings;
